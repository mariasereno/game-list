import React from 'react';
import logo from './logo.svg';
import './App.css';
import styled from 'styled-components';
import { colours } from "orca-design-system";
import {ThemeProvider} from "styled-components";
import { library } from '@fortawesome/fontawesome-svg-core';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import { Colour } from 'orca-design-system';
import { Button } from 'orca-design-system';
import { ButtonToggle } from 'orca-design-system';
import { Flex } from 'orca-design-system';
import { Layout } from 'orca-design-system';
import { Typography } from 'orca-design-system';
import { Card } from 'orca-design-system';
import { Icon } from 'orca-design-system';
import { Product } from 'orca-design-system';
library.add(far, fas);

function Demo() {
  return (
    <ThemeProvider theme={colours}>
      <div className="Demo">
      <main>
        <section>

        <Flex>
          <Colour black />
          <Colour greyDarkest />
          <Colour greyDarker />
          <Colour greyDark />
          <Colour grey />
          <Colour greyLight />
          <Colour greyLighter />
          <Colour greyLightest />
          <Colour white />
        </Flex>
        <Flex>
          <Colour primaryDarkest />
          <Colour primaryDark />
          <Colour primary />
          <Colour primaryLight />
          <Colour primaryLightest />
        </Flex>
        <Flex>
          <Colour successDarkest />
          <Colour successDark />
          <Colour success />
          <Colour successLight />
          <Colour successLightest />
        </Flex>
        <Flex>
          <Colour warningDarkest />
          <Colour warningDark />
          <Colour warning />
          <Colour warningLight />
          <Colour warningLightest />
        </Flex>
        <Flex>
          <Colour dangerDarkest />
          <Colour dangerDark />
          <Colour danger />
          <Colour dangerLight />
          <Colour dangerLightest />
        </Flex>
<Flex justifyBetween>
        <Button colour="successDark">Green button</Button>
        <Button colour="danger">Red button</Button>
        <Button colour="success">Other green button</Button>
</Flex>

        </section>
      </main>
   </div>
    </ThemeProvider>
  );
}

export default Demo;
