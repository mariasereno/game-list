import React from 'react';
import logo from './logo.svg';
import './App.css';
import styled from 'styled-components';
import { colours } from "orca-design-system";
import {ThemeProvider} from "styled-components";
import { library } from '@fortawesome/fontawesome-svg-core';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import { Button } from 'orca-design-system';
import { Flex } from 'orca-design-system';
import { Layout } from 'orca-design-system';
import { Typography } from 'orca-design-system';
import { Card } from 'orca-design-system';
import { Icon } from 'orca-design-system';
import { Product } from 'orca-design-system';
library.add(far, fas);

function App() {
  return (
    <ThemeProvider theme={colours}>
      <div className="App">
      <main>
        <section>

<div id="product-listing">
<Layout padding margin childHorizontalSpacing>
  <Flex full wrap>
      <Product width="calc(33% - 120px)" center className="product-listing-item">
        <Layout childVerticalSpacing>
          <Icon icon={["fas", "file"]} size="3x" color={colours.greyLight} />
          <Typography.H1 center>Create new</Typography.H1>
          <Typography.P center>Start with a blank slate and add your own data</Typography.P>
          <Button iconLeft><Icon icon={["fas", "shopping-cart"]} />
            Add to cart
          </Button>
        </Layout>
      </Product>
      <Product width="calc(33% - 120px)" center className="product-listing-item">
        <Layout childVerticalSpacing>
          <Icon icon={["fas", "eye"]} size="3x" color={colours.greyLight} />
          <Typography.H1 center>Explore app</Typography.H1>
          <Typography.P center>Look at pre-populated data to see app in action</Typography.P>
          <Button>
            View example
          </Button>
        </Layout>
      </Product>
      <Product width="calc(33% - 120px)" center className="product-listing-item">
      <Layout childVerticalSpacing>
          <Icon icon={["fas", "eye"]} size="3x" color={colours.greyLight} />
          <Typography.H1 center>Explore app</Typography.H1>
          <Typography.P center>Look at pre-populated data to see app in action</Typography.P>
          <Button>
            View example
          </Button>
        </Layout>
      </Product>
      <Product width="calc(33% - 120px)" center className="product-listing-item">
    <Layout childVerticalSpacing>
        <Icon icon={["fas", "eye"]} size="3x" color={colours.greyLight} />
        <Typography.H1 center>Explore app</Typography.H1>
        <Typography.P center>Look at pre-populated data to see app in action</Typography.P>
        <Button>
          View example
        </Button>
      </Layout>
      </Product>
  </Flex>
</Layout>
</div>


        </section>
      </main>
   </div>
    </ThemeProvider>
  );
}

export default App;
